const {app, Tray, Menu, nativeImage, shell} = require('electron')
const path = require('path')

let tray = null

// Timer variables
let counter = 0
let timer = null
let paused = false

function startTimer() {
    updateTimer()
    timer = setInterval(updateTimer, 1000)
}

function updateTimer() {
    counter += 1
    // counter is in seconds, we need to convert it to a string with mm:ss format
    const minutes = Math.floor(counter / 60)
    const seconds = counter % 60
    tray.setTitle(`${minutes}:${seconds < 10 ? '0' : ''}${seconds}`)
}

function pauseTimer() {
    if (timer) {
        clearInterval(timer)
        timer = null
        paused = true
    }
}

function resetTimer() {
    if (timer) {
        clearInterval(timer)
        timer = null
    }

    // Simulate calling an URL with the time counted
    console.log(`Time counted: ${counter}s`)

    counter = 0
    tray.setTitle('')
}

function buildContextMenu() {
    return Menu.buildFromTemplate([
        {
            label: timer ? 'Pause' : 'Arbeiten',
            click: () => {
                if (!timer) {
                    startTimer()
                } else {
                    pauseTimer()
                }
                tray.setContextMenu(buildContextMenu())
            }
        },
        {
            label: 'Fertig',
            click: () => {
                if (counter > 0) {
                    shell.openExternal('https://p2.faktorxmensch.com/hour/capture/' + counter)
                    resetTimer()
                    tray.setContextMenu(buildContextMenu())
                }
            },
            enabled: counter > 0
        },
    ])
}

app.on('ready', () => {
    const iconPath = path.join(__dirname, 'SolarStopwatchLinear.png')

    const trayIcon = nativeImage.createFromPath(iconPath).resize({width: 16, height: 16})

    tray = new Tray(trayIcon)

    tray.setToolTip('Time Tracker')
    tray.setContextMenu(buildContextMenu())
})
